import { FlatCompat } from '@eslint/eslintrc';

const compat = new FlatCompat({
  // import.meta.dirname is available after Node.js v20.11.0
  baseDirectory: import.meta.dirname,
});

const eslintConfig = [
  ...compat.config({
    extends: ['next/core-web-vitals', 'next/typescript', 'prettier'],
    plugins: ['simple-import-sort'],
    rules: {
      'simple-import-sort/imports': [
        'error',
        {
          groups: [
            ['^react', '^next'],
            ['^@?\\w', '^\\w'],
            ['^@components'],
            ['^@templates', '^@organisms', '^@molecules', '^@atoms'],
            ['^@stores', '^@hooks', '^@utils'],
            ['^@config', '^@const', '^@customTypes'],
            ['^@styles', '^@public', '^@types'],
            ['^[./]'],
          ],
        },
      ],
    },
  }),
];

export default eslintConfig;
