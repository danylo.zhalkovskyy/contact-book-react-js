# Contact Book App

This is a [Next.js](https://nextjs.org/) project bootstrapped with [create-next-app](https://nextjs.org/docs/app/getting-started/installation).

---

## Prerequisites

- Node.js
- npm

---

## Project setup

1. Clone the repository:
    ```
    git clone https://gitlab.com/DNL-Z/contact-book-react-js.git
    ```
2. Navigate into the project directory:
    ```
    cd contact-book-app
    ```
3. Install the dependencies:
    ```
    npm install
    ```

4. Install JSON-Server:
    ```
    npm i -g json-server
    ```

---

## Run server and app
1. Start JSON-Server (in 1st window):
    ```
    json-server --watch db.json --port 3004
    ```
2. Start the app (in 2nd window):
    ```
    npm run dev
    ```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
