import { ChangeEvent, useEffect, useState } from 'react';

import styles from '../../styles/SidePanel.module.css';

import { Contact } from '@/types/types';

export type ChangeInputEventType = ChangeEvent<HTMLInputElement>;

type Props = {
  contact?: Contact;
  isEditable: boolean;
  handleClickSubmit: (id: number, data: Contact) => void;
};

const SidePanel = (props: Props) => {
  const { contact, isEditable, handleClickSubmit } = props;
  const [state, setState] = useState<Contact>({
    id: 0,
    firstname: '',
    lastname: '',
    email: '',
    birthday: '',
  });
  const [isOpen, setIsOpen] = useState<boolean>(true);

  const handleChangeFirstname = (e: ChangeInputEventType) => {
    setState({ ...state, firstname: e.target.value });
  };

  const handleChangeLastname = (e: ChangeInputEventType) => {
    setState({ ...state, lastname: e.target.value });
  };

  const handleChangeEmail = (e: ChangeInputEventType) => {
    setState({ ...state, email: e.target.value });
    console.log(state);
  };

  const handleChangeBirthday = (e: ChangeInputEventType) => {
    setState({ ...state, birthday: e.target.value });
  };

  const submit = (data: Contact) => {
    handleClickSubmit(data.id, data);
  };

  useEffect(() => {
    if (contact) {
      setState({
        id: contact.id,
        firstname: contact.firstname,
        lastname: contact.lastname,
        email: contact.email,
        birthday: contact.birthday,
      });
    }
    console.log(contact, state);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <>
      {isOpen ? (
        <div className={styles.container}>
          <div className={styles.sidePanelContainer}>
            <input
              className={styles.input}
              type="text"
              placeholder="Firstname"
              value={state.firstname}
              onChange={handleChangeFirstname}
            />
            <input
              className={styles.input}
              type="text"
              placeholder="Lastname"
              value={state.lastname}
              onChange={handleChangeLastname}
            />
            <input
              className={styles.input}
              type="text"
              placeholder="Email"
              value={state.email}
              onChange={handleChangeEmail}
            />
            <input
              className={styles.input}
              type="text"
              placeholder="Birthday"
              value={state.birthday}
              onChange={handleChangeBirthday}
            />
            <button
              onClick={() => {
                submit(state);
              }}
            >
              Submit
            </button>
          </div>
        </div>
      ) : (
        <></>
      )}
    </>
  );
};

export default SidePanel;
