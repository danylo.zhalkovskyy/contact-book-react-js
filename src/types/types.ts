export type Contact = {
  id: number;
  firstname: string;
  lastname: string;
  email: string;
  birthday: string;
};
